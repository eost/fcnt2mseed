#!/usr/bin/env python3
import argparse
import datetime
from multiprocessing import Pool
import os
import subprocess

from obspy import UTCDateTime
from obspy import read
from obspy.io.rg16.core import _read_initial_headers


def convert(path_mseed, fcnt_file, starttime, endtime,
            network, contacts_north):
    """
    Read the waveform file in fcnt format.
    Change the network name.
    Write it in mseed format in a new directory.
    """
    st = read(fcnt_file, format="rg16", starttime=starttime, endtime=endtime,
              contacts_north=contacts_north)
    if len(st) != 0:
        for tr in st:
            tr.stats.network = network
        st.write(path_mseed, format="MSEED")


def parallelize_conversion(dir_mseed, storage_directory, network, nbr_cpu,
                           contacts_north):
    """
    Parallelize the conversion in mseed format
    """
    job_list = []
    for line_dir in os.listdir(storage_directory):
        path_line_dir = os.path.join(storage_directory, line_dir)
        for file in os.listdir(path_line_dir):
            if file.endswith('.fcnt'):
                fcnt_file = os.path.join(path_line_dir, file)
                headers = _read_initial_headers(fcnt_file)
                extended_header_1 = headers['extended_headers']['1']
                start_time_ru = extended_header_1['start_time_ru']
                pick_up_time = extended_header_1['pick_up_time']
                start_date = UTCDateTime(start_time_ru.year,
                                         start_time_ru.month,
                                         start_time_ru.day)
                end_date = UTCDateTime(pick_up_time.year, pick_up_time.month,
                                       pick_up_time.day) + datetime.timedelta(days=1)
                date_processing = start_date
                while date_processing < end_date:
                    starttime = date_processing
                    endtime = date_processing + datetime.timedelta(days=1)
                    end_filename = '.' + str(starttime.julday) + '.mseed'
                    file_mseed = fcnt_file.split('/')[-1].replace('.fcnt',
                                                                  end_filename)
                    path_mseed = os.path.join(dir_mseed, file_mseed)
                    job_list.append((path_mseed, fcnt_file, starttime, endtime,
                                     network, contacts_north))
                    date_processing += datetime.timedelta(days=1)
    pool = Pool(nbr_cpu)
    pool.starmap(convert, job_list)


def archive(archive, dir_mseed):
    """
    Archive all the mseed files in an SDS archive.
    """
    list_mseed_files = []
    for file_rep in os.listdir(dir_mseed):
        if file_rep.endswith('.mseed'):
            path_mseed = os.path.join(dir_mseed, file_rep)
            list_mseed_files.append(path_mseed)
    path_list = os.path.join(dir_mseed, 'list_mseed.txt')
    with open(path_list, 'w') as my_list:
        for path_mseed in list_mseed_files:
            my_list.write(f'{path_mseed}\n')
    subprocess.run(f'dataselect -Ps -SDS {archive} @{path_list}', shell=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert waveforms in fnct\
                                     format into mseed format. Archive the\
                                     data converted in a SDS')
    parser.add_argument('storage_directory', help='path directory containing \
                        the repertories of the receiver lines.')
    parser.add_argument('network', help='network name (2 chars expected)')
    parser.add_argument('archive_dir', help='path archive')
    parser.add_argument('dir_mseed', help='directory where the waveforms\
                         in mseed will be stored')
    parser.add_argument('nbr_cpu', help='number of CPU to used to speed up\
                        conversion and archive', type=int)
    parser.add_argument('-c', '--contacts_north', default=False, const=True,
                        action='store_const', help='map the components\
                        to Z (1C, 3C), N (3C), and E (3C) as well as correct\
                        the polarity for the vertical component')
    args = parser.parse_args()
    storage_directory = args.storage_directory
    dir_mseed = args.dir_mseed
    network = args.network
    nbr_cpu = args.nbr_cpu
    path_archive = args.archive_dir
    contacts_north = args.contacts_north
    if len(network) != 2:
        raise ValueError("2 chars are expected for the network name")
    parallelize_conversion(dir_mseed, storage_directory, network, nbr_cpu,
                           contacts_north)
    archive(path_archive, dir_mseed)
